USE [Employee]
GO
/****** Object:  Table [dbo].[Masters]    Script Date: 06/11/2019 15:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Masters](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[firstname] [nvarchar](255) NULL,
	[lastname] [nvarchar](255) NULL,
	[email] [nvarchar](255) NULL,
	[Role] [nchar](10) NULL,
	[phone] [nchar](10) NULL,
	[password] [nchar](10) NULL,
	[createdAt] [datetimeoffset](7) NOT NULL,
	[updatedAt] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK__Masters__3213E83F0EA330E9] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Masters] ON
INSERT [dbo].[Masters] ([id], [firstname], [lastname], [email], [Role], [phone], [password], [createdAt], [updatedAt]) VALUES (6, N'vishal', N'patil', N'vishalpatil12@gmail.com', N'Employee  ', N'7030500587', N'vishal12  ', CAST(0x07E0017B3F36B63F0B0000 AS DateTimeOffset), CAST(0x07B0BA11CA3BBA3F0B0000 AS DateTimeOffset))
INSERT [dbo].[Masters] ([id], [firstname], [lastname], [email], [Role], [phone], [password], [createdAt], [updatedAt]) VALUES (7, N'Akash', N'Burgute', N'akashburgute34@gmail.com', N'Employee  ', N'9850964120', N'Akash123  ', CAST(0x072096A3483FC43F0B0000 AS DateTimeOffset), CAST(0x072096A3483FC43F0B0000 AS DateTimeOffset))
INSERT [dbo].[Masters] ([id], [firstname], [lastname], [email], [Role], [phone], [password], [createdAt], [updatedAt]) VALUES (8, N'Vikas', N'Patil', N'vikaspatil54@gmail.com', N'Employee  ', N'9850965460', N'vikas34   ', CAST(0x07F09526643FC43F0B0000 AS DateTimeOffset), CAST(0x07F09526643FC43F0B0000 AS DateTimeOffset))
INSERT [dbo].[Masters] ([id], [firstname], [lastname], [email], [Role], [phone], [password], [createdAt], [updatedAt]) VALUES (9, N'Nilesh', N'Sarak', N'nileshsarak86@gmail.com', N'Admin     ', N'9850964120', N'Nilesh@199', CAST(0x07001D624E4BC43F0B0000 AS DateTimeOffset), CAST(0x07608C76E453C43F0B0000 AS DateTimeOffset))
SET IDENTITY_INSERT [dbo].[Masters] OFF
/****** Object:  Table [dbo].[leaveMgts]    Script Date: 06/11/2019 15:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[leaveMgts](
	[leave_id] [int] IDENTITY(1,1) NOT NULL,
	[empid] [int] NULL,
	[Department] [nvarchar](255) NULL,
	[Resion] [nvarchar](255) NULL,
	[fromDate] [date] NULL,
	[uptoDate] [date] NULL,
	[totalDays] [nvarchar](255) NULL,
	[status] [nvarchar](255) NULL,
	[createdAt] [datetimeoffset](7) NOT NULL,
	[updatedAt] [datetimeoffset](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[leave_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[leaveMgts] ON
INSERT [dbo].[leaveMgts] ([leave_id], [empid], [Department], [Resion], [fromDate], [uptoDate], [totalDays], [status], [createdAt], [updatedAt]) VALUES (1, 7, N'IT', N'Sick Leave', CAST(0xC53F0B00 AS Date), CAST(0xCB3F0B00 AS Date), N'6', N'1', CAST(0x0770D6391547C43F0B0000 AS DateTimeOffset), CAST(0x0770D6391547C43F0B0000 AS DateTimeOffset))
INSERT [dbo].[leaveMgts] ([leave_id], [empid], [Department], [Resion], [fromDate], [uptoDate], [totalDays], [status], [createdAt], [updatedAt]) VALUES (2, 7, N'IT', N'Marriage Leave', CAST(0xE03F0B00 AS Date), CAST(0xEC3F0B00 AS Date), N'12', N'2', CAST(0x0750FD872447C43F0B0000 AS DateTimeOffset), CAST(0x0750FD872447C43F0B0000 AS DateTimeOffset))
INSERT [dbo].[leaveMgts] ([leave_id], [empid], [Department], [Resion], [fromDate], [uptoDate], [totalDays], [status], [createdAt], [updatedAt]) VALUES (3, 6, N'Computer', N'Study Leave', CAST(0xC93F0B00 AS Date), CAST(0xD03F0B00 AS Date), N'7', N'1', CAST(0x07E04B65A447C43F0B0000 AS DateTimeOffset), CAST(0x07E04B65A447C43F0B0000 AS DateTimeOffset))
INSERT [dbo].[leaveMgts] ([leave_id], [empid], [Department], [Resion], [fromDate], [uptoDate], [totalDays], [status], [createdAt], [updatedAt]) VALUES (4, 8, N'Electronic', N'Privileged Leave', CAST(0xF03F0B00 AS Date), CAST(0x0F400B00 AS Date), N'31', N'1', CAST(0x07D0B809CB47C43F0B0000 AS DateTimeOffset), CAST(0x07D0B809CB47C43F0B0000 AS DateTimeOffset))
SET IDENTITY_INSERT [dbo].[leaveMgts] OFF
