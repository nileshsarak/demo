USE [master]
GO
/****** Object:  Database [MediBook]    Script Date: 07/29/2019 19:50:16 ******/
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = N'MediBook')
BEGIN
CREATE DATABASE [MediBook] ON  PRIMARY 
( NAME = N'MediBook', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\MediBook.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'MediBook_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\MediBook_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
END
GO
ALTER DATABASE [MediBook] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MediBook].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MediBook] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [MediBook] SET ANSI_NULLS OFF
GO
ALTER DATABASE [MediBook] SET ANSI_PADDING OFF
GO
ALTER DATABASE [MediBook] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [MediBook] SET ARITHABORT OFF
GO
ALTER DATABASE [MediBook] SET AUTO_CLOSE ON
GO
ALTER DATABASE [MediBook] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [MediBook] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [MediBook] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [MediBook] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [MediBook] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [MediBook] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [MediBook] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [MediBook] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [MediBook] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [MediBook] SET  DISABLE_BROKER
GO
ALTER DATABASE [MediBook] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [MediBook] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [MediBook] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [MediBook] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [MediBook] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [MediBook] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [MediBook] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [MediBook] SET  READ_WRITE
GO
ALTER DATABASE [MediBook] SET RECOVERY SIMPLE
GO
ALTER DATABASE [MediBook] SET  MULTI_USER
GO
ALTER DATABASE [MediBook] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [MediBook] SET DB_CHAINING OFF
GO
USE [MediBook]
GO
/****** Object:  Table [dbo].[user1]    Script Date: 07/29/2019 19:50:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[user1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[user1](
	[registeruser_id] [nvarchar](50) NOT NULL,
	[registeruser_username] [nvarchar](250) NOT NULL,
	[registeruser_email] [varchar](100) NOT NULL,
	[registeruser_password] [varchar](50) NOT NULL,
	[registeruser_createdon] [datetime] NOT NULL,
	[registeruser_modifiedon] [datetime] NOT NULL,
	[registeruser_rowstate] [tinyint] NOT NULL,
 CONSTRAINT [PK_user1] PRIMARY KEY CLUSTERED 
(
	[registeruser_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[user1] ([registeruser_id], [registeruser_username], [registeruser_email], [registeruser_password], [registeruser_createdon], [registeruser_modifiedon], [registeruser_rowstate]) VALUES (N'00E11F93-673C-4CE3-8B9E-2D666312EC6B', N'nilesh', N'nilesh123@gmail.com', N'Nilesh123', CAST(0x0000A9FE012A6DE0 AS DateTime), CAST(0x0000A9FE012A6DE0 AS DateTime), 1)
INSERT [dbo].[user1] ([registeruser_id], [registeruser_username], [registeruser_email], [registeruser_password], [registeruser_createdon], [registeruser_modifiedon], [registeruser_rowstate]) VALUES (N'0CF96D33-7C44-4DE1-87C8-18E5D15771CF', N'nilesh', N'nilesh123@gmail.com', N'Nilesh123', CAST(0x0000A9FE012C443D AS DateTime), CAST(0x0000A9FE012C443D AS DateTime), 1)
INSERT [dbo].[user1] ([registeruser_id], [registeruser_username], [registeruser_email], [registeruser_password], [registeruser_createdon], [registeruser_modifiedon], [registeruser_rowstate]) VALUES (N'19219BA9-DB1B-4CF4-B6D9-B7C0989E6005', N'nilesh123@gmail.com', N'nilesh123@gmail.com', N'Nilesh123', CAST(0x0000AA1E00FC6091 AS DateTime), CAST(0x0000AA1E00FC6091 AS DateTime), 1)
INSERT [dbo].[user1] ([registeruser_id], [registeruser_username], [registeruser_email], [registeruser_password], [registeruser_createdon], [registeruser_modifiedon], [registeruser_rowstate]) VALUES (N'1952C6D4-CC40-4FCC-95D0-116561594EA2', N'Amit', N'amp32@gmail.com', N'Amit12', CAST(0x0000A9FE015B839B AS DateTime), CAST(0x0000A9FE015B839B AS DateTime), 1)
INSERT [dbo].[user1] ([registeruser_id], [registeruser_username], [registeruser_email], [registeruser_password], [registeruser_createdon], [registeruser_modifiedon], [registeruser_rowstate]) VALUES (N'4CCEB3CD-649B-4B56-B5B3-926BF0C7715E', N'Vikas', N'viki12@gmail.com', N'Viki1275', CAST(0x0000A9FE0159009B AS DateTime), CAST(0x0000A9FE0159009B AS DateTime), 1)
INSERT [dbo].[user1] ([registeruser_id], [registeruser_username], [registeruser_email], [registeruser_password], [registeruser_createdon], [registeruser_modifiedon], [registeruser_rowstate]) VALUES (N'59CB5ABF-EC44-47C8-85CD-43733279F746', N'', N'', N'', CAST(0x0000A9FE01500FBD AS DateTime), CAST(0x0000A9FE01500FBD AS DateTime), 1)
INSERT [dbo].[user1] ([registeruser_id], [registeruser_username], [registeruser_email], [registeruser_password], [registeruser_createdon], [registeruser_modifiedon], [registeruser_rowstate]) VALUES (N'69394B6C-2083-4219-B1C3-F32CEE1CCB52', N'Aniket', N'aniket12@gmail.com', N'Aniket12', CAST(0x0000AA0200B98D97 AS DateTime), CAST(0x0000AA0200B98D97 AS DateTime), 1)
INSERT [dbo].[user1] ([registeruser_id], [registeruser_username], [registeruser_email], [registeruser_password], [registeruser_createdon], [registeruser_modifiedon], [registeruser_rowstate]) VALUES (N'777DF1BC-B651-43D8-8D06-A90E30CDE5CE', N'nilesh123@gmail.com', N'nilesh123@gmail.com', N'Nilesh123', CAST(0x0000AA2E00EEF3E1 AS DateTime), CAST(0x0000AA2E00EEF3E1 AS DateTime), 1)
INSERT [dbo].[user1] ([registeruser_id], [registeruser_username], [registeruser_email], [registeruser_password], [registeruser_createdon], [registeruser_modifiedon], [registeruser_rowstate]) VALUES (N'7A389CED-3C5D-4B3C-856A-769396C3FEF1', N'Viki', N'viki12@gmail.com', N'Viki1275', CAST(0x0000A9FE0159BE80 AS DateTime), CAST(0x0000A9FE0159BE80 AS DateTime), 1)
INSERT [dbo].[user1] ([registeruser_id], [registeruser_username], [registeruser_email], [registeruser_password], [registeruser_createdon], [registeruser_modifiedon], [registeruser_rowstate]) VALUES (N'9463E6FD-C81E-4333-B098-18A371C4FC57', N'xyz', N'xyz@gmail.com', N'xyz@123', CAST(0x0000AA02010C9211 AS DateTime), CAST(0x0000AA02010C9211 AS DateTime), 1)
INSERT [dbo].[user1] ([registeruser_id], [registeruser_username], [registeruser_email], [registeruser_password], [registeruser_createdon], [registeruser_modifiedon], [registeruser_rowstate]) VALUES (N'D8742713-1A63-4DC4-A0F6-47A23D5DBB31', N'nilesh123@gmail.com', N'nilesh123@gmail.com', N'Nilesh123', CAST(0x0000AA2C00CEEBF4 AS DateTime), CAST(0x0000AA2C00CEEBF4 AS DateTime), 1)
/****** Object:  Table [dbo].[Stock1]    Script Date: 07/29/2019 19:50:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Stock1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Stock1](
	[s_id] [int] IDENTITY(1,1) NOT NULL,
	[IName] [varchar](50) NULL,
	[IDetails] [varchar](50) NULL,
	[IPrice] [varchar](50) NULL,
	[Qnty] [int] NULL,
	[CName] [varchar](50) NULL,
	[Location] [varchar](50) NULL,
	[ExDate] [date] NULL,
	[image] [varchar](50) NULL,
 CONSTRAINT [PK_Stock1] PRIMARY KEY CLUSTERED 
(
	[s_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Stock1] ON
INSERT [dbo].[Stock1] ([s_id], [IName], [IDetails], [IPrice], [Qnty], [CName], [Location], [ExDate], [image]) VALUES (44, N'Baby Powder', N'Soft Tolcum', N'130', 600, N'Emcure', N'', CAST(0xAD430B00 AS Date), N'babyPowder.jpg')
INSERT [dbo].[Stock1] ([s_id], [IName], [IDetails], [IPrice], [Qnty], [CName], [Location], [ExDate], [image]) VALUES (45, N'Anixial', N'Tablet', N'115', 2500, N'Cipla', N'', CAST(0x794A0B00 AS Date), N'_AyurvedaAnxiekalTablet.jpg')
INSERT [dbo].[Stock1] ([s_id], [IName], [IDetails], [IPrice], [Qnty], [CName], [Location], [ExDate], [image]) VALUES (47, N'Himalaya Neem', N'Face Wash', N'130', 900, N'Zen', N'', CAST(0x00420B00 AS Date), N'himalayaNeem.jpg')
INSERT [dbo].[Stock1] ([s_id], [IName], [IDetails], [IPrice], [Qnty], [CName], [Location], [ExDate], [image]) VALUES (48, N'Detool Hand Wash', N'Baby Care', N'310', 2600, N'cp', N'', CAST(0xDE4B0B00 AS Date), N'Dettol_Handwash.jpg')
INSERT [dbo].[Stock1] ([s_id], [IName], [IDetails], [IPrice], [Qnty], [CName], [Location], [ExDate], [image]) VALUES (50, N'Mediciant', N'110', N'130', 3100, N'cp', N'', CAST(0x5B950A00 AS Date), N'Medicint.png')
INSERT [dbo].[Stock1] ([s_id], [IName], [IDetails], [IPrice], [Qnty], [CName], [Location], [ExDate], [image]) VALUES (52, N'Shabdil', N'Antibatic', N'190', 3100, N'Emcure', N'', CAST(0x5B950A00 AS Date), N'Shabdil.jpg')
SET IDENTITY_INSERT [dbo].[Stock1] OFF
/****** Object:  Table [dbo].[S1]    Script Date: 07/29/2019 19:50:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[S1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[S1](
	[s_id] [int] IDENTITY(1,1) NOT NULL,
	[CName] [varchar](50) NULL,
	[Location] [varchar](50) NULL,
 CONSTRAINT [PK_S1] PRIMARY KEY CLUSTERED 
(
	[s_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[S1] ON
INSERT [dbo].[S1] ([s_id], [CName], [Location]) VALUES (1, N'cp', N'kothrud')
INSERT [dbo].[S1] ([s_id], [CName], [Location]) VALUES (2, N'Ns', N'Thane')
INSERT [dbo].[S1] ([s_id], [CName], [Location]) VALUES (3, N'Cd', N'Mumbai')
INSERT [dbo].[S1] ([s_id], [CName], [Location]) VALUES (4, N'cp', N'')
SET IDENTITY_INSERT [dbo].[S1] OFF
/****** Object:  Table [dbo].[Order]    Script Date: 07/29/2019 19:50:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Order]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Order](
	[Order_id] [int] IDENTITY(1,1) NOT NULL,
	[s_id] [int] NOT NULL,
	[IName] [varchar](100) NULL,
	[Qnty] [int] NULL,
	[IPrice] [float] NULL,
	[fname] [varchar](100) NULL,
	[Lname] [varchar](100) NULL,
	[email] [varchar](100) NULL,
	[mobile] [int] NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Order_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mycompany]    Script Date: 07/29/2019 19:50:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Mycompany]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Mycompany](
	[c_id] [int] IDENTITY(1,1) NOT NULL,
	[Cname] [varchar](50) NULL,
	[Address] [varchar](50) NULL,
	[Persion] [varchar](50) NULL,
	[Mobile] [varchar](50) NULL,
 CONSTRAINT [PK_Mycompany] PRIMARY KEY CLUSTERED 
(
	[c_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Mycompany] ON
INSERT [dbo].[Mycompany] ([c_id], [Cname], [Address], [Persion], [Mobile]) VALUES (1, N'cp', N'Pune', N'Nilesh', N'7030500574')
INSERT [dbo].[Mycompany] ([c_id], [Cname], [Address], [Persion], [Mobile]) VALUES (2, N'Ns', N'Pune', N'Amol', N'7776865620')
INSERT [dbo].[Mycompany] ([c_id], [Cname], [Address], [Persion], [Mobile]) VALUES (3, N'Cd', N'Latur', N'Nishant', N'7776985020')
SET IDENTITY_INSERT [dbo].[Mycompany] OFF
/****** Object:  Table [dbo].[InvoiceTable]    Script Date: 07/29/2019 19:50:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[InvoiceTable](
	[ItemName] [nvarchar](50) NULL,
	[Prise] [nvarchar](50) NULL,
	[Quantity] [nvarchar](50) NULL,
	[Total] [nvarchar](50) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[InvoiceTable] ON
INSERT [dbo].[InvoiceTable] ([ItemName], [Prise], [Quantity], [Total], [Id]) VALUES (N'Baby Powder', N'250', N'3', N'750', 1)
INSERT [dbo].[InvoiceTable] ([ItemName], [Prise], [Quantity], [Total], [Id]) VALUES (N'Anixial', N'115', N'2', N'230', 10)
INSERT [dbo].[InvoiceTable] ([ItemName], [Prise], [Quantity], [Total], [Id]) VALUES (N'Antibatic', N'300', N'5', N'1500', 9)
SET IDENTITY_INSERT [dbo].[InvoiceTable] OFF
/****** Object:  Table [dbo].[Client1]    Script Date: 07/29/2019 19:50:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Client1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Client1](
	[C_id] [int] IDENTITY(1,1) NOT NULL,
	[C_fname] [varchar](50) NULL,
	[C_lname] [varchar](50) NULL,
	[C_Mobile] [bigint] NULL,
	[C_Address] [varchar](50) NULL,
	[C_city] [varchar](50) NULL,
 CONSTRAINT [PK_Client1] PRIMARY KEY CLUSTERED 
(
	[C_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Client1] ON
INSERT [dbo].[Client1] ([C_id], [C_fname], [C_lname], [C_Mobile], [C_Address], [C_city]) VALUES (1, N'Nilesh', N'Sarak', 7030500574, N'Pune', N'Pune')
INSERT [dbo].[Client1] ([C_id], [C_fname], [C_lname], [C_Mobile], [C_Address], [C_city]) VALUES (3, N'Vidya', N'Gaikwad', 7030500564, N'Pune', N'Karve Nagar')
SET IDENTITY_INSERT [dbo].[Client1] OFF
/****** Object:  Table [dbo].[C4]    Script Date: 07/29/2019 19:50:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[C4]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[C4](
	[c_id] [int] IDENTITY(1,1) NOT NULL,
	[ComN] [varchar](50) NULL,
	[Address] [varchar](50) NULL,
	[Persion] [varchar](50) NULL,
	[Mobile] [varchar](50) NULL,
 CONSTRAINT [PK_C4] PRIMARY KEY CLUSTERED 
(
	[c_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[C4] ON
INSERT [dbo].[C4] ([c_id], [ComN], [Address], [Persion], [Mobile]) VALUES (1, N'Cipla', N'pune', N'Nilesh', N'07030500574')
INSERT [dbo].[C4] ([c_id], [ComN], [Address], [Persion], [Mobile]) VALUES (2, N'Emcure', N'Hinjawadi', N'Amol', N'7776865620')
INSERT [dbo].[C4] ([c_id], [ComN], [Address], [Persion], [Mobile]) VALUES (3, N'Farmacien', N'Chinchwad', N'vki', N'9865356520')
INSERT [dbo].[C4] ([c_id], [ComN], [Address], [Persion], [Mobile]) VALUES (4, N'huihiuhk', N'knkjjn m', N'', N'')
INSERT [dbo].[C4] ([c_id], [ComN], [Address], [Persion], [Mobile]) VALUES (5, N'Zen', N'hffiwi', N'', N'')
INSERT [dbo].[C4] ([c_id], [ComN], [Address], [Persion], [Mobile]) VALUES (6, N'cp', N'pune', N'nilesh', N'070 3050 0574')
SET IDENTITY_INSERT [dbo].[C4] OFF
/****** Object:  ForeignKey [FK_Order_Order]    Script Date: 07/29/2019 19:50:18 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Order_Order]') AND parent_object_id = OBJECT_ID(N'[dbo].[Order]'))
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Order] FOREIGN KEY([Order_id])
REFERENCES [dbo].[Order] ([Order_id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Order_Order]') AND parent_object_id = OBJECT_ID(N'[dbo].[Order]'))
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Order]
GO
