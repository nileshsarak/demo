USE [master]
GO
/****** Object:  Database [Employee]    Script Date: 07/10/2019 10:48:28 ******/
CREATE DATABASE [Employee] ON  PRIMARY 
( NAME = N'Employee', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\Employee.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Employee_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\Employee_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Employee] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Employee].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Employee] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [Employee] SET ANSI_NULLS OFF
GO
ALTER DATABASE [Employee] SET ANSI_PADDING OFF
GO
ALTER DATABASE [Employee] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [Employee] SET ARITHABORT OFF
GO
ALTER DATABASE [Employee] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [Employee] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [Employee] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [Employee] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [Employee] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [Employee] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [Employee] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [Employee] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [Employee] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [Employee] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [Employee] SET  DISABLE_BROKER
GO
ALTER DATABASE [Employee] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [Employee] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [Employee] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [Employee] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [Employee] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [Employee] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [Employee] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [Employee] SET  READ_WRITE
GO
ALTER DATABASE [Employee] SET RECOVERY SIMPLE
GO
ALTER DATABASE [Employee] SET  MULTI_USER
GO
ALTER DATABASE [Employee] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [Employee] SET DB_CHAINING OFF
GO
USE [Employee]
GO
/****** Object:  Table [dbo].[users]    Script Date: 07/10/2019 10:48:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](255) NULL,
	[DptName] [nvarchar](255) NULL,
	[salary] [nvarchar](255) NULL,
	[dob] [datetimeoffset](7) NULL,
	[createdAt] [datetimeoffset](7) NOT NULL,
	[updatedAt] [datetimeoffset](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[users] ON
INSERT [dbo].[users] ([id], [FullName], [DptName], [salary], [dob], [createdAt], [updatedAt]) VALUES (1, N'ghnc', N'vhmvm', N'', CAST(0x070000000000AC3F0B0000 AS DateTimeOffset), CAST(0x0780D2F5B16EAC3F0B0000 AS DateTimeOffset), CAST(0x0780D2F5B16EAC3F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [FullName], [DptName], [salary], [dob], [createdAt], [updatedAt]) VALUES (2, N'Nilesh Sarak', N'IT', N'', CAST(0x070000000000F21C0B0000 AS DateTimeOffset), CAST(0x07D091511370AC3F0B0000 AS DateTimeOffset), CAST(0x07D091511370AC3F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [FullName], [DptName], [salary], [dob], [createdAt], [updatedAt]) VALUES (3, N'Nilesh Sarak', N'IT', N'', CAST(0x070000000000F21C0B0000 AS DateTimeOffset), CAST(0x0740F8E11F70AC3F0B0000 AS DateTimeOffset), CAST(0x0740F8E11F70AC3F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [FullName], [DptName], [salary], [dob], [createdAt], [updatedAt]) VALUES (4, N'Nilesh Sarak', N'IT', N'', CAST(0x070000000000F21C0B0000 AS DateTimeOffset), CAST(0x07F07E512E70AC3F0B0000 AS DateTimeOffset), CAST(0x07F07E512E70AC3F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [FullName], [DptName], [salary], [dob], [createdAt], [updatedAt]) VALUES (5, N'Nilesh Sarak', N'Production', N'', CAST(0x070000000000AC3F0B0000 AS DateTimeOffset), CAST(0x0790DDFD5F70AC3F0B0000 AS DateTimeOffset), CAST(0x0790DDFD5F70AC3F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [FullName], [DptName], [salary], [dob], [createdAt], [updatedAt]) VALUES (6, N'gxn gxn', N'dgngn', N'', CAST(0x070000000000AB3F0B0000 AS DateTimeOffset), CAST(0x07500290A770AC3F0B0000 AS DateTimeOffset), CAST(0x07500290A770AC3F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [FullName], [DptName], [salary], [dob], [createdAt], [updatedAt]) VALUES (7, N'Nilesh Sarak', N'IT', N'', CAST(0x070000000000A73F0B0000 AS DateTimeOffset), CAST(0x07F08F9BF570AC3F0B0000 AS DateTimeOffset), CAST(0x07F08F9BF570AC3F0B0000 AS DateTimeOffset))
SET IDENTITY_INSERT [dbo].[users] OFF
/****** Object:  Table [dbo].[usermasters]    Script Date: 07/10/2019 10:48:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usermasters](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[firstname] [nvarchar](255) NULL,
	[lastname] [nvarchar](255) NULL,
	[email] [nvarchar](255) NULL,
	[phone] [nvarchar](255) NULL,
	[password] [nvarchar](255) NULL,
	[Role] [nvarchar](255) NULL,
	[createdAt] [datetimeoffset](7) NOT NULL,
	[updatedAt] [datetimeoffset](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[usermasters] ON
INSERT [dbo].[usermasters] ([id], [firstname], [lastname], [email], [phone], [password], [Role], [createdAt], [updatedAt]) VALUES (4, N'Nilesh', N'Sarak', N'nileshsarak14@gmail.com', N'70 3050 0574', N'nilesh123', N'Admin', CAST(0x0700B325176AB33F0B0000 AS DateTimeOffset), CAST(0x0700B325176AB33F0B0000 AS DateTimeOffset))
INSERT [dbo].[usermasters] ([id], [firstname], [lastname], [email], [phone], [password], [Role], [createdAt], [updatedAt]) VALUES (8, N'Narendra', N'Somanshi', N'narendras12@gmail.com', N'7776865620', N'narendra123', N'Employee', CAST(0x07E0017B3F36B63F0B0000 AS DateTimeOffset), CAST(0x07B0BA11CA3BBA3F0B0000 AS DateTimeOffset))
INSERT [dbo].[usermasters] ([id], [firstname], [lastname], [email], [phone], [password], [Role], [createdAt], [updatedAt]) VALUES (9, N'Shu', N'jadha', N'shubhamja123@gmail.com', N'8805986589', N'shubham123', N'Employee', CAST(0x0740FE5F5836B63F0B0000 AS DateTimeOffset), CAST(0x07801788F53ABA3F0B0000 AS DateTimeOffset))
SET IDENTITY_INSERT [dbo].[usermasters] OFF
/****** Object:  Table [dbo].[Students]    Script Date: 07/10/2019 10:48:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Students](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](255) NULL,
	[DptName] [nvarchar](255) NULL,
	[salary] [nvarchar](255) NULL,
	[dob] [datetimeoffset](7) NULL,
	[createdAt] [datetimeoffset](7) NOT NULL,
	[updatedAt] [datetimeoffset](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Students] ON
INSERT [dbo].[Students] ([id], [FullName], [DptName], [salary], [dob], [createdAt], [updatedAt]) VALUES (3, N'Abc', N'Non-IT', N'15000', CAST(0x070000000000EC2B0B0000 AS DateTimeOffset), CAST(0x07C0F898A61BAD3F0B0000 AS DateTimeOffset), CAST(0x0780E4697D77B93F0B0000 AS DateTimeOffset))
INSERT [dbo].[Students] ([id], [FullName], [DptName], [salary], [dob], [createdAt], [updatedAt]) VALUES (9, N'Viki', N'IT', N'60000', CAST(0x07000000000087130B0000 AS DateTimeOffset), CAST(0x070075A9AE1EAD3F0B0000 AS DateTimeOffset), CAST(0x076092025B32AD3F0B0000 AS DateTimeOffset))
INSERT [dbo].[Students] ([id], [FullName], [DptName], [salary], [dob], [createdAt], [updatedAt]) VALUES (10, N'gbet', N'egbb', N'25000', CAST(0x070000000000AF3F0B0000 AS DateTimeOffset), CAST(0x07701B8A2632AD3F0B0000 AS DateTimeOffset), CAST(0x07701B8A2632AD3F0B0000 AS DateTimeOffset))
INSERT [dbo].[Students] ([id], [FullName], [DptName], [salary], [dob], [createdAt], [updatedAt]) VALUES (11, N'asee', N'hhhhhhhhhh', N'22000', CAST(0x0700000000009E3F0B0000 AS DateTimeOffset), CAST(0x0780E1F62C2FB03F0B0000 AS DateTimeOffset), CAST(0x07103F4B3B2FB03F0B0000 AS DateTimeOffset))
INSERT [dbo].[Students] ([id], [FullName], [DptName], [salary], [dob], [createdAt], [updatedAt]) VALUES (12, N'it', N'aaa', N'NaN', CAST(0x070000000000AF3F0B0000 AS DateTimeOffset), CAST(0x07200134532FB03F0B0000 AS DateTimeOffset), CAST(0x07200134532FB03F0B0000 AS DateTimeOffset))
SET IDENTITY_INSERT [dbo].[Students] OFF
/****** Object:  Table [dbo].[Masters]    Script Date: 07/10/2019 10:48:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Masters](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[firstname] [nvarchar](255) NULL,
	[lastname] [nvarchar](255) NULL,
	[email] [nvarchar](255) NULL,
	[Role] [nchar](10) NULL,
	[phone] [nchar](10) NULL,
	[password] [nchar](10) NULL,
	[createdAt] [datetimeoffset](7) NOT NULL,
	[updatedAt] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK__Masters__3213E83F0EA330E9] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Masters] ON
INSERT [dbo].[Masters] ([id], [firstname], [lastname], [email], [Role], [phone], [password], [createdAt], [updatedAt]) VALUES (6, N'vishal', N'patil', N'vishalpatil12@gmail.com', N'Employee  ', N'7030500587', N'vishal12  ', CAST(0x07E0017B3F36B63F0B0000 AS DateTimeOffset), CAST(0x07B0BA11CA3BBA3F0B0000 AS DateTimeOffset))
INSERT [dbo].[Masters] ([id], [firstname], [lastname], [email], [Role], [phone], [password], [createdAt], [updatedAt]) VALUES (7, N'Akash', N'Burgute', N'akashburgute34@gmail.com', N'Employee  ', N'9850964120', N'Akash123  ', CAST(0x072096A3483FC43F0B0000 AS DateTimeOffset), CAST(0x072096A3483FC43F0B0000 AS DateTimeOffset))
INSERT [dbo].[Masters] ([id], [firstname], [lastname], [email], [Role], [phone], [password], [createdAt], [updatedAt]) VALUES (8, N'Vikas', N'Patil', N'vikaspatil54@gmail.com', N'Employee  ', N'9850965460', N'vikas34   ', CAST(0x07F09526643FC43F0B0000 AS DateTimeOffset), CAST(0x07F09526643FC43F0B0000 AS DateTimeOffset))
INSERT [dbo].[Masters] ([id], [firstname], [lastname], [email], [Role], [phone], [password], [createdAt], [updatedAt]) VALUES (9, N'Nilesh', N'Sarak', N'nileshsarak86@gmail.com', N'Admin     ', N'9850964120', N'Nilesh@199', CAST(0x07001D624E4BC43F0B0000 AS DateTimeOffset), CAST(0x07608C76E453C43F0B0000 AS DateTimeOffset))
SET IDENTITY_INSERT [dbo].[Masters] OFF
/****** Object:  Table [dbo].[leaveMgts]    Script Date: 07/10/2019 10:48:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[leaveMgts](
	[leave_id] [int] IDENTITY(1,1) NOT NULL,
	[empid] [int] NULL,
	[Department] [nvarchar](255) NULL,
	[Resion] [nvarchar](255) NULL,
	[fromDate] [date] NULL,
	[uptoDate] [date] NULL,
	[totalDays] [nvarchar](255) NULL,
	[status] [nvarchar](255) NULL,
	[createdAt] [datetimeoffset](7) NOT NULL,
	[updatedAt] [datetimeoffset](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[leave_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[leaveMgts] ON
INSERT [dbo].[leaveMgts] ([leave_id], [empid], [Department], [Resion], [fromDate], [uptoDate], [totalDays], [status], [createdAt], [updatedAt]) VALUES (1, 7, N'IT', N'Sick Leave', CAST(0xC53F0B00 AS Date), CAST(0xCB3F0B00 AS Date), N'6', N'1', CAST(0x0770D6391547C43F0B0000 AS DateTimeOffset), CAST(0x0770D6391547C43F0B0000 AS DateTimeOffset))
INSERT [dbo].[leaveMgts] ([leave_id], [empid], [Department], [Resion], [fromDate], [uptoDate], [totalDays], [status], [createdAt], [updatedAt]) VALUES (2, 7, N'IT', N'Marriage Leave', CAST(0xE03F0B00 AS Date), CAST(0xEC3F0B00 AS Date), N'12', N'1', CAST(0x0750FD872447C43F0B0000 AS DateTimeOffset), CAST(0x0750FD872447C43F0B0000 AS DateTimeOffset))
INSERT [dbo].[leaveMgts] ([leave_id], [empid], [Department], [Resion], [fromDate], [uptoDate], [totalDays], [status], [createdAt], [updatedAt]) VALUES (3, 6, N'Computer', N'Study Leave', CAST(0xC93F0B00 AS Date), CAST(0xD03F0B00 AS Date), N'7', N'1', CAST(0x07E04B65A447C43F0B0000 AS DateTimeOffset), CAST(0x07E04B65A447C43F0B0000 AS DateTimeOffset))
INSERT [dbo].[leaveMgts] ([leave_id], [empid], [Department], [Resion], [fromDate], [uptoDate], [totalDays], [status], [createdAt], [updatedAt]) VALUES (4, 8, N'Electronic', N'Privileged Leave', CAST(0xF03F0B00 AS Date), CAST(0x0F400B00 AS Date), N'31', N'1', CAST(0x07D0B809CB47C43F0B0000 AS DateTimeOffset), CAST(0x07D0B809CB47C43F0B0000 AS DateTimeOffset))
SET IDENTITY_INSERT [dbo].[leaveMgts] OFF
/****** Object:  Table [dbo].[employeeLeaves]    Script Date: 07/10/2019 10:48:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[employeeLeaves](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[empid] [int] NULL,
	[Department] [nvarchar](255) NULL,
	[Resion] [nvarchar](255) NULL,
	[fromDate] [datetimeoffset](7) NULL,
	[uptoDate] [datetimeoffset](7) NULL,
	[totalDays] [nvarchar](255) NULL,
	[createdAt] [datetimeoffset](7) NOT NULL,
	[updatedAt] [datetimeoffset](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[employeeLeaves] ON
INSERT [dbo].[employeeLeaves] ([id], [empid], [Department], [Resion], [fromDate], [uptoDate], [totalDays], [createdAt], [updatedAt]) VALUES (1, 8, N'computer', N'Seek Leave', CAST(0x070000000000B73F0B0000 AS DateTimeOffset), CAST(0x070000000000B93F0B0000 AS DateTimeOffset), N'03', CAST(0x07006896B947B63F0B0000 AS DateTimeOffset), CAST(0x07006896B947B63F0B0000 AS DateTimeOffset))
INSERT [dbo].[employeeLeaves] ([id], [empid], [Department], [Resion], [fromDate], [uptoDate], [totalDays], [createdAt], [updatedAt]) VALUES (2, 8, N'Computer', N'Study Leave', CAST(0x070000000000BD3F0B0000 AS DateTimeOffset), CAST(0x070000000000A23F0B0000 AS DateTimeOffset), N'04', CAST(0x0730A8629048B63F0B0000 AS DateTimeOffset), CAST(0x0730A8629048B63F0B0000 AS DateTimeOffset))
INSERT [dbo].[employeeLeaves] ([id], [empid], [Department], [Resion], [fromDate], [uptoDate], [totalDays], [createdAt], [updatedAt]) VALUES (6, 10, N'IT', N'Marriage Leave', CAST(0x070000000000B93F0B0000 AS DateTimeOffset), CAST(0x070000000000BE3F0B0000 AS DateTimeOffset), N'06', CAST(0x07C051C8345AB63F0B0000 AS DateTimeOffset), CAST(0x07C051C8345AB63F0B0000 AS DateTimeOffset))
INSERT [dbo].[employeeLeaves] ([id], [empid], [Department], [Resion], [fromDate], [uptoDate], [totalDays], [createdAt], [updatedAt]) VALUES (7, 9, N'Computer', N'Seek Leave', CAST(0x070000000000B93F0B0000 AS DateTimeOffset), CAST(0x070000000000BC3F0B0000 AS DateTimeOffset), N'04', CAST(0x0780BFF7F75CB63F0B0000 AS DateTimeOffset), CAST(0x0780BFF7F75CB63F0B0000 AS DateTimeOffset))
INSERT [dbo].[employeeLeaves] ([id], [empid], [Department], [Resion], [fromDate], [uptoDate], [totalDays], [createdAt], [updatedAt]) VALUES (10, 9, N'Electronics', N'Study Leave', CAST(0x070000000000BA3F0B0000 AS DateTimeOffset), CAST(0x070000000000A03F0B0000 AS DateTimeOffset), N'05', CAST(0x07708CDFB545B73F0B0000 AS DateTimeOffset), CAST(0x07708CDFB545B73F0B0000 AS DateTimeOffset))
INSERT [dbo].[employeeLeaves] ([id], [empid], [Department], [Resion], [fromDate], [uptoDate], [totalDays], [createdAt], [updatedAt]) VALUES (25, 8, N'IT', N'Study Leave', CAST(0x070000000000B83F0B0000 AS DateTimeOffset), CAST(0x070000000000B93F0B0000 AS DateTimeOffset), N'01', CAST(0x07B031B57859B83F0B0000 AS DateTimeOffset), CAST(0x07B031B57859B83F0B0000 AS DateTimeOffset))
SET IDENTITY_INSERT [dbo].[employeeLeaves] OFF
