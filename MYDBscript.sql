USE [master]
GO
/****** Object:  Database [MyDB]    Script Date: 07/10/2019 10:43:19 ******/
CREATE DATABASE [MyDB] ON  PRIMARY 
( NAME = N'MyDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\MyDB.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'MyDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\MyDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [MyDB] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MyDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MyDB] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [MyDB] SET ANSI_NULLS OFF
GO
ALTER DATABASE [MyDB] SET ANSI_PADDING OFF
GO
ALTER DATABASE [MyDB] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [MyDB] SET ARITHABORT OFF
GO
ALTER DATABASE [MyDB] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [MyDB] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [MyDB] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [MyDB] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [MyDB] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [MyDB] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [MyDB] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [MyDB] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [MyDB] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [MyDB] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [MyDB] SET  DISABLE_BROKER
GO
ALTER DATABASE [MyDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [MyDB] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [MyDB] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [MyDB] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [MyDB] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [MyDB] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [MyDB] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [MyDB] SET  READ_WRITE
GO
ALTER DATABASE [MyDB] SET RECOVERY SIMPLE
GO
ALTER DATABASE [MyDB] SET  MULTI_USER
GO
ALTER DATABASE [MyDB] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [MyDB] SET DB_CHAINING OFF
GO
USE [MyDB]
GO
/****** Object:  Table [dbo].[users]    Script Date: 07/10/2019 10:43:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[firstname] [nvarchar](255) NULL,
	[lastname] [nvarchar](255) NULL,
	[email] [nvarchar](255) NULL,
	[password] [nvarchar](255) NULL,
	[createdAt] [datetimeoffset](7) NOT NULL,
	[updatedAt] [datetimeoffset](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[users] ON
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (4, N'Nilesh', N'Sarak', N'nileshsarak14@gmail.com', N'nilesh123', CAST(0x07107558D484AF3F0B0000 AS DateTimeOffset), CAST(0x07107558D484AF3F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (5, N'Nilesh', N'Sarak', N'nileshsarak14@gmail.com', N'nilesh123', CAST(0x07B06027F284AF3F0B0000 AS DateTimeOffset), CAST(0x07B06027F284AF3F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (6, N'Amol', N'sarak', N'nileshsarak14@gmail.com', N'amol111', CAST(0x07F0B554E385AF3F0B0000 AS DateTimeOffset), CAST(0x07F0B554E385AF3F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (7, NULL, NULL, NULL, NULL, CAST(0x07F07864D354B03F0B0000 AS DateTimeOffset), CAST(0x07F07864D354B03F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (8, NULL, NULL, NULL, NULL, CAST(0x07C008E3D055B03F0B0000 AS DateTimeOffset), CAST(0x07C008E3D055B03F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (9, NULL, NULL, NULL, NULL, CAST(0x072036DD3456B03F0B0000 AS DateTimeOffset), CAST(0x072036DD3456B03F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (10, NULL, NULL, NULL, NULL, CAST(0x07F028BB9856B03F0B0000 AS DateTimeOffset), CAST(0x07F028BB9856B03F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (11, NULL, NULL, NULL, NULL, CAST(0x079039F7C956B03F0B0000 AS DateTimeOffset), CAST(0x079039F7C956B03F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (12, NULL, NULL, NULL, NULL, CAST(0x0750792B5457B03F0B0000 AS DateTimeOffset), CAST(0x0750792B5457B03F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (13, NULL, NULL, NULL, NULL, CAST(0x07D0F0D75557B03F0B0000 AS DateTimeOffset), CAST(0x07D0F0D75557B03F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (14, NULL, NULL, NULL, NULL, CAST(0x071009039157B03F0B0000 AS DateTimeOffset), CAST(0x071009039157B03F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (15, NULL, NULL, NULL, N'admin', CAST(0x0700AE6FA357B03F0B0000 AS DateTimeOffset), CAST(0x0700AE6FA357B03F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (16, NULL, NULL, NULL, N'admin', CAST(0x074001381A58B03F0B0000 AS DateTimeOffset), CAST(0x074001381A58B03F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (17, NULL, NULL, NULL, N'admin', CAST(0x0740622C7E58B03F0B0000 AS DateTimeOffset), CAST(0x0740622C7E58B03F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (18, NULL, NULL, NULL, N'admin', CAST(0x071017420B59B03F0B0000 AS DateTimeOffset), CAST(0x071017420B59B03F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (19, NULL, NULL, NULL, N'admin', CAST(0x07D02CC58959B03F0B0000 AS DateTimeOffset), CAST(0x07D02CC58959B03F0B0000 AS DateTimeOffset))
INSERT [dbo].[users] ([id], [firstname], [lastname], [email], [password], [createdAt], [updatedAt]) VALUES (20, NULL, NULL, NULL, N'admin', CAST(0x0720AAA0995AB03F0B0000 AS DateTimeOffset), CAST(0x0720AAA0995AB03F0B0000 AS DateTimeOffset))
SET IDENTITY_INSERT [dbo].[users] OFF
/****** Object:  Table [dbo].[UserMasters]    Script Date: 07/10/2019 10:43:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserMasters](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](255) NULL,
	[password] [nvarchar](255) NULL,
	[createdAt] [datetimeoffset](7) NOT NULL,
	[updatedAt] [datetimeoffset](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[UserMasters] ON
INSERT [dbo].[UserMasters] ([id], [username], [password], [createdAt], [updatedAt]) VALUES (1, N'admin', N'adminL', CAST(0x07D0C0C9B855B03F0B0000 AS DateTimeOffset), CAST(0x07D0C0C9B855B03F0B0000 AS DateTimeOffset))
SET IDENTITY_INSERT [dbo].[UserMasters] OFF
